#include <stdio.h>
int choice;
int c1, c2, result;
float divr;

int add(int x1,int x2);
int subtract(int x1, int x2);
int multiply(int x1, int x2);
float divide(int x1, int x2);

int main(void){

    printf("Hello sir, what function would you like to perform today?\n1. Add\n2. Subtract\n3. Multiply\n4. Division\n");
    scanf("%d",&choice);
    if(choice==1){
        printf("Please give me 2 numbers to add together!\n");
        scanf("%d%d",&c1,&c2);
        printf("Your result is: %d",add(c1,c2));
    }
    else if(choice==2){
        printf("Please give me 2 numbers to subtract!\n");
        scanf("%d%d",&c1,&c2);
        printf("Your result is: %d",subtract(c1,c2));
    }
    else if(choice==3){
        printf("Please give me 2 numbers to multiply together!\n");
        scanf("%d%d",&c1,&c2);
        printf("Your result is: %d",multiply(c1,c2));
    } else if(choice==4){
        printf("Please give me 2 numbers to divide together!\n");
        scanf("%d%d",&c1,&c2);
        printf("Your result is: %f",divide(c1,c2));
    }
    else{
        printf("Please enter a valid input!");
    }
}

int add(int x1, int x2){
  result = x1+x2;
  return result;
}
int subtract(int x1, int x2){
    result = x1-x2;
    return result;
}
int multiply(int x1, int x2){
    result = x1*x2;
    return result;
}
float divide(int x1, int x2){
    divr = (float)x1/x2;
    return divr;
}
